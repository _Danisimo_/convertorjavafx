package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private TextField fieldTime;

    @FXML
    private TextField fieldWeight;

    @FXML
    private TextField fieldVulume;

    @FXML
    private TextField fieldLength;

    @FXML
    private TextField fieldTemperature;


    @FXML
    Spinner<String> spinnerTime = new Spinner<>();
    ObservableList<String> time = FXCollections.observableArrayList(//
             "Min","Hour","Day", "Week","Month","Asrtonomical Year", "Third");

    @FXML
    Spinner<String> spinnerWeight = new Spinner<>();
    ObservableList<String> weight = FXCollections.observableArrayList(//
            "g","c","carat","eng pound","stone","rus pound", "Third");

    @FXML
    Spinner<String> spinnerVolume = new Spinner<>();
    ObservableList<String> volume = FXCollections.observableArrayList(//
            "m^3","gallon","pint","quart","barrel","cubic foot", "cubic inch");

    @FXML
    Spinner<String> spinnerLength = new Spinner<>();
    ObservableList<String> length = FXCollections.observableArrayList(//
            "km","mile","nautical mile","cable","league","foot", "yard");
    @FXML
    Spinner<String> spinnerTemperature = new Spinner<>();
    ObservableList<String> temperature = FXCollections.observableArrayList(//
            "K (Шкала Кельвина)","F (Шкала Фаренгейта)","Re (Шкала Реомюра)","Ro (Шкала Рёмер)","Ra (Шкала Ранкина)"," N (Шкала Ньютона", "D (Шкала Дели́ля)");


    @FXML
    private void inTime(){}

    @FXML
    private void ofTime(){}

    @FXML
    private void inWeight(){}

    @FXML
    private void ofWeight(){}

    @FXML
    private void inVolume(){}

    @FXML
    private void ofVolume(){}

    @FXML
    private void inLength(){}

    @FXML
    private void ofLength(){}

    @FXML
    private void inTemperature(){}

    @FXML
    private void ofTemperature(){}

    public void Initalize(){
        SpinnerValueFactory<String> valueTime = new SpinnerValueFactory.ListSpinnerValueFactory<>(time);

    }


}

