package main;

import javafx.fxml.FXML;
import javafx.scene.control.*;


public class Controller {

    @FXML
    TextField enterValueFrom;

    @FXML
    TextField enterValueTo;

    @FXML
    ComboBox<Category> category = new ComboBox<>();

    @FXML
    ComboBox<String> convertFrom = new ComboBox<>();

    @FXML
    ComboBox<String> convertTo = new ComboBox<>();

    @FXML
    public void initialize() {
        category.getItems().setAll(Category.values());

        //category.getSelectionModel().selectedItemProperty().addListener( (v,oldValue, newValue )-> convertFrom.getItems().setAll(Category.getAllUnits(newValue)));
        category.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> convertFrom.setValue(Category.getUnit0(newValue)));

        category.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> convertTo.getItems().setAll(Category.getAllUnits(newValue)));
        category.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> convertTo.setValue(Category.getUnit1(newValue)));

    }

    @FXML
    private void changeCategoryAction() {
        enterValueFrom.setText("");
        enterValueTo.setText("");

    }

    @FXML
    private void convert() {
        Boolean firstNumToSecondNum = false;

        String categoryOfMetrics = parseComBoBox(category);

        String firstMetric = parseComBoBox(convertFrom);
        String secondMetric = parseComBoBox(convertTo);

        double firstNum = 0;
        double secondNum = 0;

        if (enterValueFrom.isFocused()) {
            firstNum = parseTextField(enterValueFrom);
            firstNumToSecondNum = true;
        } else {
            secondNum = parseTextField(enterValueTo);
        }

        switch (categoryOfMetrics) {
            case "LENGTH":
                if (firstNumToSecondNum) {
                    enterValueTo.setText(Function.lenthConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    enterValueFrom.setText(Function.lenthConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "TEMPERATURE":
                if (firstNumToSecondNum) {
                    enterValueTo.setText(Function.temperatureConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    enterValueFrom.setText(Function.temperatureConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "WEIGHT":
                if (firstNumToSecondNum) {
                    enterValueTo.setText(Function.weightConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    enterValueFrom.setText(Function.weightConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "TIME":
                if (firstNumToSecondNum) {
                    enterValueTo.setText(Function.timeConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    enterValueFrom.setText(Function.timeConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "VOLUME":
                if (firstNumToSecondNum) {
                    enterValueTo.setText(Function.volumeConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    enterValueFrom.setText(Function.volumeConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
        }
    }

    @FXML
    public Double parseTextField(TextField textField) {

        try {
            Double number = Double.parseDouble(textField.getText());
            return number;
        } catch (Exception e) {
            return 0.0;
        }
    }

    @FXML
    public String parseComBoBox(ComboBox comboBox) {

        String unitValue = comboBox.getValue().toString();

        return unitValue;
    }

}


